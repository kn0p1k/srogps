<!DOCTYPE HTML>
<html>
    <head>
        <link rel="stylesheet" href="http://srogps.ru/templates/generous/css/bootstrap.css">
        <link rel="stylesheet" href="http://srogps.ru/templates/generous/css/theme.css">
        <link rel="stylesheet" href="http://srogps.ru/templates/generous/css/static.css">
        <link rel="stylesheet" href="http://srogps.ru/templates/generous/css/custom.css">
        <style>
            html,body{
                height:100%;
                background-color: #fff;
            }
            .news_date{
                font-size: 10pt;
                font-weight: bold;
            }
            .news_text{
                padding: 10px;
            }
            .catItemReadMore{ 

            }
        </style>
        <script>
        function showFull(id){
            window.open("news.php?full="+id,"Новости");
        }
        </script>
    </head>
    <body>
        <?php
        include 'sql/db.php';
        $isFull = filter_input(INPUT_GET, 'full', FILTER_VALIDATE_INT);

        if ($isFull) {
            $sql = "SELECT id,text,date FROM news where id=:id";
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':id', $isFull, PDO::PARAM_INT);
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo "<div id='news_" . $row['id'] . "' class='news_text'>";
                echo "<span class='news_date'>" . split(' ', $row['date'])[0] . "</span><br>";
                echo preg_replace('/src="([^"]+)"/', 'src="sql/get_img.php?id=\1&news=1"', $row['text']);
                echo "</div><hr>";
            }
        } else {
            $sql = "SELECT id,preview,date FROM news order by date desc";
            $stmt = $db->prepare($sql);
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo "<div id='news_" . $row['id'] . "' class='news_text'>";
                echo "<span class='news_date'>" . split(' ', $row['date'])[0] . "</span><br>";
                echo preg_replace('/src="([^"]+)"/', 'src="sql/get_img.php?id=\1&news=1"', $row['preview']);
                echo "<div class='catItemReadMore'>
			<a class='k2ReadMore readon' href='' onclick='showFull(".$row['id'].")'>Подробнее ...</a>
		</div>";
                echo "</div><hr>";
            }
        }
        ?>
    </body>
</html>