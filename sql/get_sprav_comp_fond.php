<?php

include 'db.php';
$success = false;

// запрос даты
$sql = "select date_format(dat,'%d.%m.%Y %H:%i:%s') as dat from dates where id= 2";
$stmt = $db->prepare($sql);
$stmt->execute();

$dat = "";

while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $dat = stripslashes($row['dat']);
}

// запрос на 1 таблицу
$sql = "SELECT type, count FROM kf_count";
$stmt = $db->prepare($sql);
$stmt->execute();

$tab1 = array();

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $success = true;
    $tab1[] = array(
        'type' => stripslashes($row['type'])
        , 'count' => stripslashes($row['count'])
    );
}

// запрос на 3 таблицу верхняя часть
$sql = "SELECT sum, count(*) cnt, sum(sum) itog FROM kf_reestr group by sum order by sum desc";
$stmt = $db->prepare($sql);
$stmt->execute();

$tab3top = array();

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $tab3top[] = array(
        'sum' => stripslashes($row['sum'])
        , 'cnt' => stripslashes($row['cnt'])
        , 'itog' => stripslashes($row['itog'])
    );
}

// нижняя часть
$sql = "SELECT god,val FROM kf_percent";
$stmt = $db->prepare($sql);
$stmt->execute();

$tab3bottom = array();

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $tab3bottom[] = array(
        'god' => stripslashes($row['god'])
        , 'val' => stripslashes($row['val'])
    );
}

// нижняя часть
$sql = "select sum(val) val from (SELECT val FROM kf_percent 
                                union all
                              select sum val from kf_reestr
                              ) t";
$stmt = $db->prepare($sql);
$stmt->execute();

$tab3itog = array();

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $tab3itog[] = array(
        'val' => stripslashes($row['val'])
    );
}


// запрос на 4 таблицу
$sql = "SELECT num, nam, opf, inn, sum FROM admin_srogps.kf_reestr";
$stmt = $db->prepare($sql);
$stmt->execute();

$tab4 = array();

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $tab4[] = array(
        'num' => stripslashes($row['num'])
        ,'nam' => stripslashes($row['nam'])
        ,'opf' => stripslashes($row['opf'])
        ,'inn' => stripslashes($row['inn'])
        ,'sum' => stripslashes($row['sum'])
    );
}


$out = array(
    "success" => $success
    , "dat" => $dat
    , "tab1" => $tab1
    , "tab3top" => $tab3top
    , "tab3bottom" => $tab3bottom
    , "tab3itog" => $tab3itog
    , "tab4" => $tab4
);

// отправляем в ответ
echo json_encode($out);
