<?php

$dsn = 'mysql:dbname=admin_srogps;host=localhost';
$user = 'root';
$password = '';

try {
    
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
    );
    
    $db = new PDO($dsn, $user, $password, $options);
    $db->exec("SET group_concat_max_len=400000");
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
}
?>
