<?php

include 'db.php';
$success = false;

$sql = "SELECT parent,name FROM jobs where type=:type and company=:id group by parent,name order by id";

$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
$type = filter_input(INPUT_GET, 'type', FILTER_VALIDATE_INT);

$stmt = $db->prepare($sql);
$stmt->bindValue(':id', $id, PDO::PARAM_INT);
$stmt->bindValue(':type', $type, PDO::PARAM_INT);

$stmt->execute();
$data = array();

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $success = true;
    $data[] = array(
        'parent' => stripslashes($row['parent'])
        ,'name' => stripslashes($row['name'])
        
    );
}


$out = array(
    "success" => $success,
    "rows" => $data
);

// отправляем в ответ
echo json_encode($out);
