<?php
include 'db.php';
$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
$name = filter_input(INPUT_GET, 'name', FILTER_SANITIZE_MAGIC_QUOTES);

$sql = "SELECT file,body FROM companys where id=:id";
$stmt = $db->prepare($sql);
$stmt->bindValue(':id', $id, PDO::PARAM_INT);
$stmt->execute();

if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $arrTmp = explode('.', $row["file"]);
    $ext = $arrTmp[count($arrTmp)-1];
    
    switch ($ext) {
        case "docx":
            header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            header('Content-Disposition: attachment;filename="'.$name.'.docx"');
            header('Cache-Control: max-age=0');
            break;
	case "doc":
            header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            header('Content-Disposition: attachment;filename="'.$name.'.doc"');
            header('Cache-Control: max-age=0');
            break;
        case "jpg":
            header('Content-Type: image/jpeg');
			header('Cache-Control: max-age=0');

        default:
            break;
    }
    echo $row['body'];   
    exit;
    
}else{
    echo "Нет такого файла";
}
