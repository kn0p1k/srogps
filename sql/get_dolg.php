<?php

include 'db.php';
$success = false;

$sql = "select nam name, type, val from credits where val <> 0 order by type, val desc";
$stmt = $db->prepare($sql);

$stmt->execute();
$data = array();

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $success = true;
    $data[] = array(        
        'name' => stripslashes($row['name'])
        ,'type' => stripslashes($row['type'])
        ,'val' => stripslashes($row['val'])
        
    );
}

$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
$sql = "select date_format(dat,'%d.%m.%Y %H:%i:%s') as dat from dates where id= 1";
$stmt = $db->prepare($sql);
$stmt->bindValue(':id', $id, PDO::PARAM_INT);
$stmt->execute();

$dat = "";

while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $dat = stripslashes($row['dat']);
}

$out = array(
    "success" => $success,
    "dat" => $dat,
    "rows" => $data
);

// отправляем в ответ
echo json_encode($out);