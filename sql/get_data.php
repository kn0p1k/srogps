<?php
mb_internal_encoding("UTF-8");
error_reporting(E_ALL);
ini_set("display_errors", 1);

include 'db.php';
$success = false;

$sql = "select * from (select `c`.`id` AS `key`,"
        . " `d2`.`val` AS `id`,"
        . " `c`.`name` AS `name`,"
        . " date_format(`c`.`date_upd`, '%d.%m.%Y') AS `date_upd`,"
        . " group_concat(distinct `d1`.`val`"
        . "    order by `d1`.`ord` ASC"
        . "    separator '###') AS `attr6`,"
        . " group_concat(distinct `d2`.`val`"
        . "    order by `d2`.`ord` ASC"
        . "    separator '###') AS `attr43`,"
        . " group_concat(distinct `d3`.`val`"
        . "    order by `d3`.`ord` ASC"
        . "    separator '###') AS `attr11`,"
        . " group_concat(distinct `d4`.`val`"
        . "    order by `d4`.`ord` ASC"
        . "    separator '###') AS `attr10`,"
        . " group_concat(distinct `d5`.`val`"
        . "    order by `d5`.`ord` ASC"
        . "    separator '###') AS `attr3`,"
        . " group_concat(distinct `d6`.`val`"
        . "    order by `d6`.`ord` ASC"
        . "    separator '###') AS `attr4`,"
        . " group_concat(distinct `d7`.`val`"
        . "    order by `d7`.`ord` ASC"
        . "    separator '###') AS `attr50`,"
        . " group_concat(distinct `d8`.`val`"
        . "    order by `d8`.`ord` ASC"
        . "    separator '###') AS `attr41`,"
        . " group_concat(distinct `d9`.`val`"
        . "    order by `d9`.`ord` ASC"
        . "    separator '###') AS `attr12`,"
        . " group_concat(distinct `d10`.`val`"
        . "    order by `d10`.`ord` ASC"
        . "    separator '###') AS `attr44`,"
        . " group_concat(distinct `d11`.`val`"
        . "    order by `d11`.`ord` ASC"
        . "    separator '###') AS `attr45`,"
        . " group_concat(distinct `d12`.`val`"
        . "    order by `d12`.`ord` ASC"
        . "    separator '###') AS `attr28`,"
        . " group_concat(distinct `d13`.`val`"
        . "    order by `d13`.`ord` ASC"
        . "    separator '###') AS `attr46`,"
        . " group_concat(distinct `d14`.`val`"
        . "    order by `d14`.`ord` ASC"
        . "    separator '###') AS `attr47`,"
        . " group_concat(distinct `d15`.`val`"
        . "    order by `d15`.`ord` ASC"
        . "    separator '###') AS `attr55`,"
        . " group_concat(distinct `d16`.`val`"
        . "   order by `d16`.`ord` ASC"
        . "    separator '###') AS `attr13`,"
        . " group_concat(distinct `d17`.`val`"
        . "    order by `d17`.`ord` ASC"
        . "    separator '###') AS `attr8`,"
        . " group_concat(distinct `d18`.`val`"
        . "    order by `d18`.`ord` ASC"
        . "    separator '###') AS `attr7`,"
        . " group_concat(distinct `d19`.`val`"
        . "    order by `d19`.`ord` ASC"
        . "    separator '###') AS `attr49`,"
        . " group_concat(distinct `d20`.`val`"
        . "    order by `d20`.`ord` ASC"
        . "    separator '###') AS `attr56`,"
        . " group_concat(distinct `d21`.`val`"
        . "    order by `d21`.`ord` ASC"
        . "    separator '###') AS `attr51`,"
        . " group_concat(distinct `d22`.`val`"
        . "   order by `d22`.`ord` ASC"
        . "    separator '###') AS `attr48`,"
        . " group_concat(distinct `d23`.`val`"
        . "    order by `d23`.`ord` ASC"
        . "    separator '###') AS `attr29`,"
        . " group_concat(distinct `d24`.`val`"
        . "    order by `d24`.`ord` ASC"
        . "    separator '###') AS `attr30`,"
        . " group_concat(distinct `d25`.`val`"
        . "    order by `d25`.`ord` ASC"
        . "    separator '###') AS `attr81`,"
        . " group_concat(distinct `d26`.`val`"
        . "    order by `d26`.`ord` ASC"
        . "    separator '###') AS `attr82`"
        . " from"
        . " ((((((((((((((((((((((((((`companys` `c`"
        . " left join `companys_data` `d1` ON (((`c`.`id` = `d1`.`company`)"
        . "    and (`d1`.`attr` = 6))))"
        . " left join `companys_data` `d2` ON (((`c`.`id` = `d2`.`company`)"
        . "    and (`d2`.`attr` = 43))))"
        . " left join `companys_data` `d3` ON (((`c`.`id` = `d3`.`company`)"
        . "    and (`d3`.`attr` = 11))))"
        . " left join `companys_data` `d4` ON (((`c`.`id` = `d4`.`company`)"
        . "    and (`d4`.`attr` = 10))))"
        . " left join `companys_data` `d5` ON (((`c`.`id` = `d5`.`company`)"
        . "    and (`d5`.`attr` = 3))))"
        . " left join `companys_data` `d6` ON (((`c`.`id` = `d6`.`company`)"
        . "    and (`d6`.`attr` = 4))))"
        . " left join `companys_data` `d7` ON (((`c`.`id` = `d7`.`company`)"
        . "    and (`d7`.`attr` = 50))))"
        . " left join `companys_data` `d8` ON (((`c`.`id` = `d8`.`company`)"
        . "    and (`d8`.`attr` = 41))))"
        . " left join `companys_data` `d9` ON (((`c`.`id` = `d9`.`company`)"
        . "    and (`d9`.`attr` = 12))))"
        . " left join `companys_data` `d10` ON (((`c`.`id` = `d10`.`company`)"
        . "    and (`d10`.`attr` = 44))))"
        . " left join `companys_data` `d11` ON (((`c`.`id` = `d11`.`company`)"
        . "    and (`d11`.`attr` = 45))))"
        . " left join `companys_data` `d12` ON (((`c`.`id` = `d12`.`company`)"
        . "    and (`d12`.`attr` = 28))))"
        . " left join `companys_data` `d13` ON (((`c`.`id` = `d13`.`company`)"
        . "    and (`d13`.`attr` = 46))))"
        . " left join `companys_data` `d14` ON (((`c`.`id` = `d14`.`company`)"
        . "    and (`d14`.`attr` = 47))))"
        . " left join `companys_data` `d15` ON (((`c`.`id` = `d15`.`company`)"
        . "    and (`d15`.`attr` = 55))))"
        . " left join `companys_data` `d16` ON (((`c`.`id` = `d16`.`company`)"
        . "    and (`d16`.`attr` = 13))))"
        . " left join `companys_data` `d17` ON (((`c`.`id` = `d17`.`company`)"
        . "    and (`d17`.`attr` = 8))))"
        . " left join `companys_data` `d18` ON (((`c`.`id` = `d18`.`company`)"
        . "   and (`d18`.`attr` = 7))))"
        . " left join `companys_data` `d19` ON (((`c`.`id` = `d19`.`company`)"
        . "    and (`d19`.`attr` = 49))))"
        . " left join `companys_data` `d20` ON (((`c`.`id` = `d20`.`company`)"
        . "    and (`d20`.`attr` = 56))))"
        . " left join `companys_data` `d21` ON (((`c`.`id` = `d21`.`company`)"
        . "    and (`d21`.`attr` = 51))))"
        . " left join `companys_data` `d22` ON (((`c`.`id` = `d22`.`company`)"
        . "    and (`d22`.`attr` = 48))))"
        . " left join `companys_data` `d23` ON (((`c`.`id` = `d23`.`company`)"
        . "    and (`d23`.`attr` = 29))))"
        . " left join `companys_data` `d24` ON (((`c`.`id` = `d24`.`company`)"
        . "    and (`d24`.`attr` = 30))))"
        . " left join `companys_data` `d25` ON (((`c`.`id` = `d25`.`company`)"
        . "    and (`d25`.`attr` = 81))))"
        . " left join `companys_data` `d26` ON (((`c`.`id` = `d26`.`company`)"
        . "    and (`d26`.`attr` = 82))))"
    	. " group by `c`.`id` , `c`.`name`) t where 1=1";

$nomer = filter_input(INPUT_GET, 'nomer', FILTER_SANITIZE_MAGIC_QUOTES);
$nazvanie = filter_input(INPUT_GET, 'nazvanie', FILTER_SANITIZE_MAGIC_QUOTES);
$status = filter_input(INPUT_GET, 'status', FILTER_SANITIZE_MAGIC_QUOTES);
$inn = filter_input(INPUT_GET, 'inn', FILTER_SANITIZE_MAGIC_QUOTES);
$ogrn = filter_input(INPUT_GET, 'ogrn', FILTER_SANITIZE_MAGIC_QUOTES);
$strah = filter_input(INPUT_GET, 'strah', FILTER_SANITIZE_MAGIC_QUOTES);
$rukovod = filter_input(INPUT_GET, 'rukovod', FILTER_SANITIZE_MAGIC_QUOTES);
$vidrab = filter_input(INPUT_GET, 'vidrab', FILTER_SANITIZE_MAGIC_QUOTES);

//$key = filter_input(INPUT_GET, 'key', FILTER_VALIDATE_INT);
//$name = filter_input(INPUT_GET, 'name', FILTER_SANITIZE_MAGIC_QUOTES);
// РґРѕР±Р°РІР»СЏРµРј РїР°СЂР°РјРµС‚СЂС‹ РІ Р·Р°РїСЂРѕСЃ РёСЃС…РѕРґСЏ РёР· РїСЂРёС€РµРґС€РёС… РґР°РЅРЅС‹С…
if ($nomer != null) {
    $sql .= " and `id` like :nomer";
}
if ($nazvanie != null) {
    $sql .= " and name like :nazvanie";
}
if ($status != null) {
    $sql .= " and attr55 like :status";
}
if ($inn != null) {
    $sql .= " and attr3 like :inn";
}
if ($ogrn != null) {
    $sql .= " and attr4 like :ogrn";
}
if ($strah != null) {
    $sql .= " and attr28 like :strah";
}
if ($rukovod != null) {
    $sql .= " and attr13 like :rukovod";
}
if ($vidrab != null) {
    $sql .= " and t.`key` in (select distinct company from  jobs where name like :vidRab) ";
}

$sql .= " order by attr43";
//echo $sql."<br>";
$stmt = $db->prepare($sql);

// РїСЂРёС†РµРїР»СЏРµРј Р·РЅР°С‡РµРЅРёСЏ
if ($nomer != null) {
    $stmt->bindValue(':nomer', "%" . $nomer ."%", PDO::PARAM_INT);
}
if ($nazvanie != null) {
    $stmt->bindValue(':nazvanie', "%" . $nazvanie . "%", PDO::PARAM_STR);
}
if ($status != null) {
    $stmt->bindValue(':status', "%" . $status . "%", PDO::PARAM_STR);
}
if ($inn != null) {
    $stmt->bindValue(':inn', "%" . $inn . "%", PDO::PARAM_STR);
}
if ($ogrn != null) {
    $stmt->bindValue(':ogrn', "%" . $ogrn . "%", PDO::PARAM_STR);
}
if ($strah != null) {
    $stmt->bindValue(':strah', "%" . $strah . "%", PDO::PARAM_STR);
}
if ($rukovod != null) {
    $stmt->bindValue(':rukovod', "%" . $rukovod . "%", PDO::PARAM_STR);
}
if ($vidrab != null) {
    $stmt->bindValue(':vidRab', "%" . $vidrab . "%", PDO::PARAM_STR);
}

$stmt->execute();
$data = array();

// С„РѕСЂРјРёСЂСѓРµРј РјР°СЃСЃРёРІ РґР°РЅРЅС‹С…
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $success = true;
    $data[] = array(
        'key' => stripslashes($row['key']),
        'id' => stripslashes($row['id']),
        'name' => stripslashes($row['name']),
        'date_upd' => stripslashes($row['date_upd']),
        'attr3' => stripslashes($row['attr3']),
        'attr4' => stripslashes($row['attr4']),
        'attr41' => stripslashes($row['attr41']),
        'attr6' => stripslashes($row['attr6']),
        'attr7' => stripslashes($row['attr7']),
        'attr8' => stripslashes($row['attr8']),
        'attr10' => stripslashes($row['attr10']),
        'attr11' => stripslashes($row['attr11']),
        'attr12' => stripslashes($row['attr12']),
        'attr13' => stripslashes($row['attr13']),
        'attr28' => stripslashes($row['attr28']),
        'attr29' => stripslashes($row['attr29']),
        'attr30' => stripslashes($row['attr30']),
        'attr43' => stripslashes($row['attr43']),
        'attr44' => stripslashes($row['attr44']),
        'attr45' => stripslashes($row['attr45']),
        'attr46' => stripslashes($row['attr46']),
        'attr47' => stripslashes($row['attr47']),
        'attr48' => stripslashes($row['attr48']),
        'attr49' => stripslashes($row['attr49']),
        'attr50' => stripslashes($row['attr50']),
        'attr51' => stripslashes($row['attr51']),
        'attr55' => stripslashes($row['attr55']),
        'attr56' => stripslashes($row['attr56']),
        'attr81' => stripslashes($row['attr81']),
        'attr82' => stripslashes($row['attr82'])
    );
}

$out = array(
    "success" => $success,
    "rows" => $data
);

// РѕС‚РїСЂР°РІР»СЏРµРј РІ РѕС‚РІРµС‚
echo json_encode($out);
