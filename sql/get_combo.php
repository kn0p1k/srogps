<?php

include 'db.php';
$success = false;

$sql = "SELECT val, count(*) cnt FROM companys_data where attr=55 group by val";
$stmt = $db->prepare($sql);

$stmt->execute();
$data = array();

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $success = true;
    $data[] = array(
        'val' => stripslashes($row['val']),
        'cnt' => stripslashes($row['cnt'])
    );
}

$out = array(
    "success" => $success,
    "rows" => $data
);

// отправляем в ответ
echo json_encode($out);