<?php
include 'db.php';
$sql = "SELECT logo,filename FROM logo where id=:id";

$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
$isNews = filter_input(INPUT_GET, 'news', FILTER_VALIDATE_INT);

if($isNews){
    $sql = "SELECT img,filename FROM news_img where id=:id";
}


$stmt = $db->prepare($sql);
$stmt->bindValue(':id', $id, PDO::PARAM_INT);
$stmt->execute();

if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $arrTmp = explode('.', $row["filename"]);
    $ext = $arrTmp[count($arrTmp)-1];
    
    switch ($ext) {        
        case "jpg":
            header('Content-Type: image/jpeg');
            header('Cache-Control: max-age=0');
            break;
        case "jpeg":
            header('Content-Type: image/jpeg');
            header('Cache-Control: max-age=0');
            break;
        case "png":
            header('Content-Type: image/png');
            header('Cache-Control: max-age=0');
            break;
        case "gif":
            header('Content-Type: image/gif');
            header('Cache-Control: max-age=0');
            break;
        default:
            break;
    }
    if($isNews){
        echo $row['img'];
    }else{
        echo $row['logo'];   
    }
    exit;
    
}else{
    header('Content-Type: image/gif');
    header('Cache-Control: max-age=0');
    echo file_get_contents('../img/blank.gif');
    exit;
}
